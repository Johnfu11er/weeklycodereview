"""
Write a function that given an input string with parenthesis (), brackets [], and curly braces {}, determine if all characters are properly nested/paired.
Make sure your function can take a string with regular characters in addition to the characters above.

Examples:
'{[]}'   -> True
'{[])    -> False  (Should end in a closing curly brace, not a closing parenthesis)
'}{'     -> False  (String began with a closing curly brace, but a pair must begin with an opening character such as '(', '[', or '{' )


Lastly:
Find/run the pip command in a terminal that will tell you if the pytest module is installed.
Install it if not already installed.  
Now find/run the pip command that will allow you to see the versions of all installed modules.
Using the pytest module, use the hard_test.py script to test your function against the myriad of test cases.
"""



def is_paired(input_string: str) -> bool:
    pass
            





def main():
    """ Put all your testing code here"""
    pass


if __name__ == '__main__':
    main()