"""
Create a GenericNode class.
Give it one class variable, and two instance variables.
The class variable will be a domain name of your choosing, and the two instance variables will be a hostname and IP address.
Add a new method that will allow you to call your class using the str() built in method.  It should return the hostname.

"""


class GenericNode:
    pass



def main():
    """ Put all your testing code here"""
    pass




if __name__ == '__main__':
    main()