"""
Import your generic node class from easy.py

Create a new class called Server and make it inherit from the GenericNode class
Add an additional instance variable called operating_system.  
Additionally, create an fqdn attribute that is derived from the hostname and the domain name.
Create this attribute in a way where if the hostname is changed after an instance is created, the fqdn will change to reflect that.  

Create another new class called Router and make it inherit from the GenericNode class
Instead of an operating_system instance variable, it should have a network_os instance variable
Overwrite the __str__ method and have it return the IP address instead of the hostname

"""




def main():
    """ Put all your testing code here"""
    pass


if __name__ == '__main__':
    main()