"""
Given a maze represented by a grid, create a function to return the solution as a list of points.
Points with 1's represent valid paths and points with 0's represent walls.
You can assume that it will always start at the top left corner and finish at the bottom right corner of the maze.



The x coordinate gets higher as you move from left to right.
The y coordinate gets higher as you move from top to bottom.

The top let corner is (0,0).
In this example, the bottom right coordinate of the maze would be (3,3)

maze = [            
    [1,1,1,1],
    [0,1,0,0],
    [0,1,0,0],
    [0,1,1,1]
]


solution: [ (0,0), (1,0), (1,1), (1,2), (1,3), (2,3), (3,3) ]
"""

def solve(maze):
    pass


def main():
    """ Put all your testing code here. """
    pass

if __name__ == '__main__':
    main()