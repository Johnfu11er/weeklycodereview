network_devices = {
    'R_001' : 
        {
            'name' : 'R_001',
            'device_type' : 'Router',
            'manufacturer' : 'Cisco'
        },
    'R_002' : 
        {
            'name' : 'R_002',
            'device_type' : 'Router',
            'manufacturer' : 'Juniper'
        },
    'SW_001' : 
        {
            'name' : 'SW_001',
            'device_type' : 'Switch',
            'manufacturer' : 'Cisco'
        },
    'SW_002' : 
        {
            'name' : 'SW_002',
            'device_type' : 'Switch',
            'manufacturer' : 'Arista'
        }
}

def device_data(device):
    '''
    Returns the name,  manufacturer, and device type for the 
    argument 'device'
    '''
    # todo: write the function and remove the 'pass' command below
    pass

# todo: write a print statement that calls the function to display your results