![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=jinja&logoColor=white&color=2bbc8a)

<h1>Week 4 (28 Oct)</h1>

<details>
    <summary>Easy Challenge</summary>
Using the existing 'network_devices' dictionary, write a function that returns the network device name, manufacturer, and device_type
</details><br>

<details>
    <summary> Medium Challenge</summary>
Using the existing API call, write a function that returns the network device name, manufacturer, and device_type
</details><br>

<details>
    <summary>Hard Challenge</summary>
Write a function that returns the network device name, manufacturer, and device_type using the API address below:<br>

```python
url = "https://gitlab.com/Johnfu11er/weeklycodereviewsupport/-/raw/main/more_network_devices.json"
```

</details><br>
<details>
    <summary>Resources</summary>
 - <a href="https://docs.python-requests.org/en/latest/">Python Requests Package</a><br>
 - <a href="https://docs.python.org/3/library/json.html">Python JSON Package</a><br>
 - <a href="https://www.programiz.com/python-programming/json">Practical JSON uses in Python</a><br>
 - <a href="https://www.geeksforgeeks.org/python-dictionary/">Python dictionary basics</a>
</details><br>