''' This exercise will familiarize you with the other python request verbs. We will begin with creating a customer on the gorest.co API. 
The getCustomer function will then retrieve your newly created customer and display it. Notice: getCustomer is looking for a parameter called "cusId". 
Afterwards, you will need to send a PATCH request to modify the customer you created. Make sure you visit the README for more detailed instructions.
'''

# To do: Import modules

def getCustomer(cusId):
    url = "https://gorest.co.in/public/v1/users/" + str(cusId)
    headers = {
        "Content-type" : "application/json",
        "Accept": "application/json"
        }
    output = requests.get(url, headers=headers).json()
    easy_read = json.dumps(output, indent = 4)
    print(easy_read)


def postCustomer():
    
    # todo: Post a new customer and return the output

    pass



def patchCustomer():
    

    # todo: Patch the customer you created with a new name.

    pass


def main():

    getCustomer(cusId)



if __name__ == "__main__":
    main()

