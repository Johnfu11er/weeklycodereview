<h1> Week 5 </h1>

<details> 
<summary>Easy Exercise </summary>
 
 In this exercise we are breaking down the composition of a GET request. This function will also be used for later exercises.
 The API endpoint in this case is a Joke api (https://sv443.net/jokeapi/v2/joke/). Feel free to familiarize yourself with the API itself through your browser.
 
 Theres no tasks in this exercise. Just run the function and analyze the output. Notice the data structure in the output.

 </details><br>

 <details>
 <summary> Medium Exercise </summary>
 In this exercise:

 1. Import the function from Easy.py
 
 2. Complete the function "parseJson()". 
    
 End Result: We want everything stripped out of the JSON response except the actual joke itself.

 3. Run the the function

</details><br>

<details>
<summary> Hard Exercise </summary>

In this exercise we are using another free API (https://gorest.co.in/). This API will allow us to use the other verbs in the python Requests library (i.e, POST, PUT, PATCH, DELETE). 

 1. First you will need to request a token from the site. You will need to create an account in order to authenticate and request a token. You will need a token in order to POST, PUT, PATCH, or DELETE. This token will be in your header as a "Bearer token". 

 2. Specify the following information in the data of your request: name, email, status, and gender. You will need to format this data in a json string (json.dumps). REMEMBER this is an open API that anyone can see! Email will need to be unique everytime you send a post. Otherwise it will give a 422 error.

 3. Return the output of your POST request in JSON format.

 4. From the POST request, retrieve the JSON object associated with the "id".

 5. Assign this object to a variable named "cusId" in the main function.

 6. Now we are going to "GET" that id with the getCustomer function with cusId as a parameter. The getCustomer function will print the JSON object with that id.

 7. After you analyze the output, you realize you named it wrong! Write another function that patches the customer you just made to reflect a different name.

 8. Reusability of code is key! Run the getCustomer(cusId) function again and check out your new name!

 </details>