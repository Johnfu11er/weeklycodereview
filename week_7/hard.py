"""
Without replicating code, use your function from easy.py to save the data in easy.json to a variable called 'router'.
Use your function to also save the data in hard.json as a different variable called 'routers'
Add the router variable to the routers list.  
Using a list comprehension, create a list of device names from the 'routers' variable.
"""

"""
OPTIONAL:  
After using a list comprehension successfully, create a function that can do the same thing for any desired key.
If any of the routers doesn't have the key, return None.

Examples:

print(get_key_values(router_list, key_name='name')) -> ['router_1', 'router_2', 'router_3', 'router_4', 'router_5']
print(get_key_values(router_list, key_name='platform')) -> ['cisco', 'cisco', 'cisco', None, None]

"""



# Optional challenge
def get_key_values(list_of_dicts, key_name):
    """ Add a doc string here """
    pass


def main():
    """ Put all your testing code here"""

    device_names = []
    


if __name__ == '__main__':
    main()