"""
In easy.json, create a json-formatted dictionary that represents a router and has a device_type, device_role, and a platform. Look at hard.json for examples of what the values might be.
Create a function that takes a json file and returns a python data structure.
Use your function to save the dictionary from easy.json as a variable called 'router'.

Remember, cool kids use type hints and doc strings

OPTIONAL:
Use try/except to return a custom message if the file is not found.
"""







def from_json_file(file_path):
    """ Add a doc string here """
    pass


def main():
    """ Put all your testing code here"""
    pass
    


if __name__ == '__main__':
    main()
