"""
Create a recursive function to provide you with the Nth number of the fibonacci sequence
The fibonacci sequence starts with 1, 1, and each number after that is the sum of the two previous numbers.
For example, after 1, 1, the next number would be the sum of 1 + 1, so the third number in the sequence is 2.
Now the sequence is 1, 1, 2.  Since the two previous numbers are 1 and 2, we would take their sum to find the next number in the sequence, which would be 3.

Here's what the beginning of the sequence looks like:
1,1,2,3,5,8,13,21,34

Your function will return the Nth number of this sequence.  

Example:

fib(2) -> 1 (Second number in the sequence)
fib(4) -> 3 (Fourth number in the sequence)
fib(7) -> 13 (Seventh number in the sequence)
"""

def fib(n):
    pass




def main():
    """ Put all your testing code here """
    pass



if __name__ == '__main__':
    main()