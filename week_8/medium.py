"""
Instructions
Given a DNA strand, return its RNA complement (per RNA transcription).

Both DNA and RNA strands are a sequence of nucleotides.

The four nucleotides found in DNA are adenine (A), cytosine (C), guanine (G) and thymine (T).

The four nucleotides found in RNA are adenine (A), cytosine (C), guanine (G) and uracil (U).

Given a DNA strand, its transcribed RNA strand is formed by replacing each nucleotide with its complement:

G -> C
C -> G
T -> A
A -> U

FOR TESTING:
Use the pytest module and the 'medium_test.py' file to test your function
It's important that this file name remains medium and the function name below remains 'to_rna'
"""


def to_rna(dna_strand):
    pass


def main():
    """ Put all your testing code here """
    pass


if __name__ == '__main__':
    main()