"""
Create a function to find the cartesian product of two iterables

Examples:
[1, 2] * [1, 2]      -> [1, 2, 2, 4]
[1, 2] * [1, 2, 3]   -> [1, 2, 3, 2, 4, 6]
"""


def cartesian(iterable_a, iterable_b):
    pass


def main():
    """ Put all your testing code here """
    pass


if __name__ == '__main__':
    main()